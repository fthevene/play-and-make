Play And Make
=============

Projet pour l'UV SI28
---------------------

Jeu multijoueur asynchrone développé en JavaScript avec le framework phaser.io

Serveur
-------
Installer node.js
Dans une console, dans le dossier du projet:

$ npm install
permet d'installer les dépendances

$ npm start
permet de lancer le serveur
