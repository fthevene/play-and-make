var Niveau = function(niveauInit) {
    var niveau = niveauInit || [];

    return {
        at: function(x, y) {
            for(var ti = 0; ti < niveau.length; ti++) {
                var tile = niveau[ti];
                if (tile[0] === x && tile[1] === y) {
                    return tile[2];
                }
            }
            return undefined;
        },

        removeAt: function(x, y) {
            niveau = niveau.filter(function(tile) {
                return tile[0] !== x && tile[0] !== y;
            });
        },

        setAt: function(x, y, block) {
            for(var ti = 0; ti < niveau.length; ti++) {
                var tile = niveau[ti];
                if (tile[0] === x && tile[1] === y) {
                    tile[2] = block;
                    return;
                }
            }
            niveau.push([x, y, block]);
        },

        get: function() {
            return niveau;
        },
    };
};

var StrokeType = { REMOVE: 0, ADD_REPLACE: 1 };
var marker;

function Editor(game, common, request, auth) {
    var map, layer, marker;
    var niveau, currentBlock, niveauSaveId;
    var strokeType, path;
    var cursors;
    var plateformes;

    return {
        preload: function() {
            common.preload_blocks(game);
        },

        init: function(niveauInit, niveauId) {
            if (niveauInit) {
                niveau = Niveau(niveauInit);
                niveauSaveId = niveauId;
            } else {
                niveau = Niveau();
                niveauSaveId = null;
            }
        },

        create: function() {
            this.create_map();
            common.create_blocks(map);

            this.create_layer();
            this.create_marker();
            this.handle_mouse_events();

            currentBlock = 0;

            this.create_tile_selector();

            cursors = game.input.keyboard.createCursorKeys();

            plateformes = game.add.group();
	    plateformes.enableBody = true;
            common.create_sol(plateformes);

            this.create_test_buttons();
            this.create_save_button();

            game.input.mouse.mouseWheelCallback = function(event) {
                if (game.input.mouse.wheelDelta == Phaser.Mouse.WHEEL_UP) {
                    game.camera.x -= 10;
                } else {
                    game.camera.x += 10;
                }
            };
        },

        create_map: function() {
            map = game.add.tilemap(null, common.BLOCK_SIZE, common.BLOCK_SIZE, common.BLOCKS_X, common.BLOCKS_Y);
            game.stage.backgroundColor = common.BACKGROUND;
        },

        create_layer: function() {
            layer = map.createBlankLayer('plateformes', common.BLOCKS_X, common.BLOCKS_Y, common.BLOCK_SIZE, common.BLOCK_SIZE);

            layer.resizeWorld();

            layer.alpha = 1;

            layer.inputEnabled = true;
            layer.priorityID = 0;

            common.create_niveau(map, layer, niveau.get());
        },

        create_marker: function() {
            marker = game.add.graphics();
            marker.lineStyle(2, 0x000000, 1);
            marker.drawRect(0, 0, 32, 32);
        },

         create_tile_selector: function() {
             var add_button = function(posX, bloc) {
                 var bt = game.add.button(0, 0, bloc, this.set_current_block, this);
                 bt.fixedToCamera = true;
                 bt.cameraOffset.setTo(posX, 0);
                 bt.priorityID = 1;
                 bt.input.useHandCursor = true;
             };
             add_button.call(this,  0 + 128, 'bloc');
             add_button.call(this, 32 + 128, 'bloc_blanc');
             add_button.call(this, 64 + 128, 'but');
             add_button.call(this, 96 + 128, 'mechant');
             add_button.call(this, 128 + 128, 'dirt');


             var t = game.add.text(0, 0, 'Blocks :', { fontSize: '32px', fill: '#000' });
             t.fixedToCamera = true;
             t.cameraOffset.setTo(0, 0);
        },

        set_current_block: function(x){
            switch(x.key) {
            case 'but':
                currentBlock = 1;
                marker.lineStyle(2, 0xFFF200, 1);
                break;
            case 'mechant':
                currentBlock = 2;
                marker.lineStyle(2, 0xF60000, 1);
                break;
            case 'bloc':
                currentBlock = 0;
                marker.lineStyle(2, 0x000000, 1);
                break;
            case 'bloc_blanc':
                currentBlock = 3;
                marker.lineStyle(2, 0xFFFFFF, 1);
                break;
            case 'dirt':
                currentBlock = 4;
                marker.lineStyle(2, 0x996600, 1);
                break;
            default:
                return true;
            }
            
            marker.drawRect(0, 0, 32, 32);
        },

        handle_mouse_events: function() {
            layer.events.onInputDown.add(this.handle_mouse_down, this);
        },

        create_test_buttons: function() {
            var button_test = game.add.text(0, 0, 'Try this level');
            button_test.fixedToCamera = true;
            button_test.cameraOffset.setTo(128 + 196, 0);
            button_test.inputEnabled = true;
            button_test.priorityID = 1;
            button_test.input.useHandCursor = true;
            button_test.events.onInputDown.add(function() {
                game.state.start('Game', true, false, niveauSaveId, niveau.get(), true);
            });
        },

        create_save_button: function() {
            var button_save = game.add.text(0, 0, 'Save');
            button_save.fixedToCamera = true;
            button_save.cameraOffset.setTo(128 + 430, 0);
            button_save.inputEnabled = true;
            button_save.priorityID = 1;
            button_save.input.useHandCursor = true;

            var menu = this;
            button_save.events.onInputDown.add(function() {
                if (!auth.isAuthenticated()) {
                    common.loginForm("Please register to save a level", auth,
                                     function(userInfos) {
                                         menu.save_level();
                                     });
                } else {
                    this.save_level();
                }
            }, this);
        },

        save_level: function() {
            if (niveauSaveId == null) {
                var name = auth.getName() + " " + Math.floor(Math.random() * 1001); // TODO: ask user
                console.log("save level " + name);
                request.post("/server/niveau", {name: name, categorie: "a", data: niveau.get()})
                    .then(function(error, text, xhr) {
                        if (xhr.status == 201) {
                            niveauSaveId = JSON.parse(text).id;
                            alert("Niveau sauvegardé !");
                        }
                    });
            } else {
                request.post("/server/niveau/" + niveauSaveId, {data: niveau.get()})
                    .then(function(error, text, xhr) {
                        if (xhr.status == 200) {
                            alert("Niveau sauvegardé");
                        }
                    });
            }
        },
        
        update: function() {
            var x = layer.getTileX(game.input.activePointer.worldX);
            var y = layer.getTileY(game.input.activePointer.worldY);

            if (x < common.BLOCKS_X && y < common.BLOCKS_Y - 1 && y > 0) {
                marker.x = x * common.BLOCK_SIZE;
                marker.y = y * common.BLOCK_SIZE;
            } else {
                marker.x = -100;
                marker.y = -100;
                return;
            }

            if (cursors.left.isDown)
            {
                game.camera.x -= 4;
            }
            else if (cursors.right.isDown)
            {
                game.camera.x += 4;
            }
        },

        handle_mouse_down: function() {
            var x = layer.getTileX(game.input.activePointer.worldX);
            var y = layer.getTileY(game.input.activePointer.worldY);

            strokeType = this.toggle_block(x, y);
            path = [[x, y]];

            game.input.addMoveCallback(this.draw_blocks, this);
            layer.events.onInputUp.addOnce(this.handle_mouse_up);
        },

        handle_mouse_up: function() {
            game.input.deleteMoveCallback(0); // bug bizarre de Phaser, il faut mettre 0 au lieu de this.handle_mouse_up
            strokeType = undefined;
            path = [];
        },

        remove_block: function(x, y) {
            niveau.removeAt(x, y);
            map.removeTile(x, y, layer);
            return StrokeType.REMOVE;
        },

        replace_block: function(x, y) {
            niveau.setAt(x, y, currentBlock);
            map.putTile(currentBlock, x , y, layer);
            return StrokeType.ADD_REPLACE;
        },

        toggle_block: function (x, y) {
            if (niveau.at(x, y) == currentBlock) {
                return this.remove_block(x, y);
            } else {
                return this.replace_block(x, y);
            }
        },

        draw_blocks: function() {
            var x = layer.getTileX(game.input.activePointer.worldX);
            var y = layer.getTileY(game.input.activePointer.worldY);

            if (!path.some(function(point) { return point[0] == x && point[1] == y; })) {
                path.push([x, y]);
                if (strokeType === StrokeType.ADD_REPLACE) {
                    this.replace_block(x, y);
                } else if (strokeType === StrokeType.REMOVE) {
                    this.remove_block(x, y);
                }
            }
        }
    };
};
