function Auth(request) {
    var waitingForAuth = false,
        userInfos = null;

    var auth = {
        register: function(name, mail, callback) {
            request.post("/server/user/register", {name: name, email: mail})
                .then(function(error, text, xhr) {
                    if (xhr.status == 201) {
                        userInfos = {name: name, email: mail};
                        callback(false, userInfos);
                    } else {
                        callback(xhr.status, JSON.parse(text));
                    }
                });
        },

        isAuthenticated: function() {
            return userInfos !== null;
        },

        getName: function() {
            return userInfos.name;
        }
    };

    waitingForAuth = true;
    request.get("/server/user/info")
        .then(function(error, text, xhr) {
            waitingForAuth = false;
            
            if (xhr.status == 200) {
                try {
                    var userData = JSON.parse(text);
                    if (userData.name && userData.email) {
                        userInfos = userData;
                    }
                } catch(e) {
                    console.log(text);
                }
            }
        });
    
    return auth;
};
