var width = window.innerWidth;
var height = window.innerHeight;

// On cache la scrollbar
document.body.style.overflow = 'hidden';

var game = new Phaser.Game(width, height, Phaser.CANVAS, 'gameCanvas');

var request = {
    post: function(url, data) {
        return promise.post(url, JSON.stringify(data), {"Accept": "application/json",
                                                        "Content-Type": "application/json"});
    },

    get: function(url, data) {
        return promise.get(url, data, {"Accept": "application/json"});
    }
};

var auth = Auth(request);

game.state.add('Menu', Menu(game, common, request, auth));
game.state.add('Game', Game(game, common, request, auth));
game.state.add('Editor', Editor(game, common, request, auth));

game.state.start('Menu');
