function Game(game, common, request) {
    var plateformes;
    var spawnx = 0;
    var spawny = (common.BLOCKS_Y - 2) * common.BLOCK_SIZE; // 2 en moins pour le sol
    var score = 0;
    var scoreTexte;
    var menuMort;
    var mechants;
    var listeMechants;
    var joueur, fpsTexte;
    var layers = { plateformes: null, but: null, mort: null };
    var map;

    var double_saut = -1;

    var niveau, niveauId = null;
    var canGoBackToEditor;

    var music;
    var timer;
    
    var counter = 0;

    var deciseconds = 0,
        seconds = 0,
        minutes = 0;

    var zones = {up: {pressed: false, rect: new Phaser.Rectangle()},
                 right: {pressed: false, rect: new Phaser.Rectangle()},
                 left: {pressed: false, rect: new Phaser.Rectangle()}};

    return {
	preload : function () {
            common.preload_blocks(game);
	    game.load.image('gars', 'assets/gars.png');
	    game.load.image('pause', 'assets/pause.png');
	    game.load.image('menuPause', 'assets/menuPause.png');
	    game.load.image('mort', 'assets/mort.png');
	    game.load.image('reset', 'assets/reset.png');
	    game.load.image('win', 'assets/win.png');

	},

        init: function(id, niveauInit, fromEditor) {
            console.log(id, niveauInit, fromEditor);
            niveauId = id;
            niveau = niveauInit;
            canGoBackToEditor = fromEditor;
        },
        
	create : function () {
	    //On active les physiques "arcades"
	    game.physics.startSystem(Phaser.Physics.ARCADE);

	    //On défini le fond
	    game.stage.backgroundColor = common.BACKGROUND;

	    //Pour redimensionner le jeu
	    window.addEventListener("resize",function(){
	        game.scale.setGameSize(window.innerWidth, window.innerHeight);
                this.update_touch_zones();
	    }, this);

	    // Pour les FPS
	    game.time.advancedTiming = true;

	    // create tilemap
	    map = game.add.tilemap(null, common.BLOCK_SIZE, common.BLOCK_SIZE, common.BLOCKS_X, common.BLOCKS_Y);

	    // declare tiles
            common.create_blocks(map);

	    // Groupe des plateformes
	    plateformes = game.add.group();
	    plateformes.enableBody = true;

	    // create tilemap layer, 32px*32px tiles, 60x25 tiles
	    layers.plateformes = map.createBlankLayer('plateformes', common.BLOCKS_X, common.BLOCKS_Y, common.BLOCK_SIZE, common.BLOCK_SIZE, plateformes);
            layers.plateformes.renderSettings.enableScrollDelta = false;

	    layers.plateformes.resizeWorld();

	    layers.plateformes.alpha = 1;

	    // put tiles
            common.create_niveau(map, layers.plateformes, niveau);

	    map.setLayer(layers.plateformes);
	    map.setCollisionBetween(0, 100); // whatever. le second argument donne jusqu'à quel ID de bloc la collision sera prise en compte

	    map.setCollision(3, false);		//n'autorise pas la collision avec le bloc blanc (id=3)

		map.setTileIndexCallback(1, this.gagnerJeu, this, layers.plateformes);
		map.setTileIndexCallback(2, this.perdreJeu, this, layers.plateformes);

	   map.setCollision(4, true);			//avec le bloc dirt

	    //Sol
            common.create_sol(plateformes);

	    //Le joueur
	    joueur = game.add.sprite(spawnx, spawny, 'gars');
	    game.physics.enable(joueur);
	    game.camera.follow(joueur);

	    // deadzone (rectangle dans lequel le joueur peut bouger sans faire bouger la caméra)
	    // on fait suivre la caméra à partir d'un septième de la largeur de la vue
	    game.camera.deadzone = new Phaser.Rectangle(common.VISIBLE_X * common.BLOCK_SIZE / 7, 0, common.VISIBLE_X * common.BLOCK_SIZE / 5, common.BLOCKS_Y * common.BLOCK_SIZE);
	    // éventuellement à agrandir quand on avance plus vite (touche droit)

	    joueur.body.gravity.y = 1500;
	    joueur.body.collideWorldBounds = true;

	    //Score
	    scoreTexte = game.add.text(0, 0, 'Score:', { fontSize: '32px', fill: '#000' });
	    scoreTexte.fixedToCamera = true;
	    scoreTexte.cameraOffset.setTo(16, 0);

	    // FPS
	    fpsTexte = game.add.text(0, 0, 'FPS: 0', { fontSize : '32px', fill: '#000' });
	    fpsTexte.fixedToCamera = true;
	    fpsTexte.cameraOffset.setTo(150, 0);

	    // Timer
	    timer = game.add.text(0, 0, 'Temps: 00:00:0', { fontSize : '32px', fill: '#000' });
	   	timer.fixedToCamera = true;
	    timer.cameraOffset.setTo(600,0);
	    Phaser.inputEnabled = true;
	    this.currentTimer = game.time.create(false);

	    //Bouton pause
	    var pauseBouton = game.add.sprite(0, 0, 'pause');
	    pauseBouton.fixedToCamera = true;
	    pauseBouton.cameraOffset.setTo(common.VISIBLE_X * common.BLOCK_SIZE - 48, 0);
	    pauseBouton.inputEnabled = true;
	    pauseBouton.events.onInputUp.add(function() {
	        game.paused = true;
	    });

	    var unpause = function(event) {
	        // Only act if paused
	        if(game.paused) {

	            // Check if the click was inside the menu
	            if(event.x > 1300-48 && event.x < 1300-16 && event.y > 16 && event.y < 16+32 ){

	                //menuPause.destroy();
	                game.paused = false;

	            }
	        }
	    };
	    game.input.onDown.add(unpause, this);

	    //Message de mort

	    //contrôles
	    cursors = game.input.keyboard.createCursorKeys();
	    jumpButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

            if (canGoBackToEditor) {
                this.create_editor_button();
            }

            this.update_touch_zones();
            this.enable_touch();

	    // this.currentTimer.loop(Phaser.Timer.SECOND, this.updateTimer, this);	//Update every s
	    this.currentTimer.loop(100, this.updateTimer, this);	//Update every 100ms = 0,1s
	    // this.currentTimer.loop(10, this.updateTimer, this);	//Update every 10ms = 0,01s Mais ça lag (trop lent)
	    this.currentTimer.start();
	},

        enable_touch: function() {
            layers.plateformes.inputEnabled = true;
            layers.plateformes.input.maxPointers = 2;
            layers.plateformes.events.onInputDown.add(this.handle_touch_event, this);
            layers.plateformes.events.onInputUp.add(this.handle_touch_event, this);
        },

        handle_touch_event: function() {
            var pm = game.input.mousePointer,
                p1 = game.input.pointer1,
                p2 = game.input.pointer2;
            
            zones.up.pressed =    (p1.isDown && zones.up.rect.contains(p1.x, p1.y)) ||
                                  (p2.isDown && zones.up.rect.contains(p2.x, p2.y)) ||
                                  (pm.isDown && zones.up.rect.contains(pm.x, pm.y));
            zones.right.pressed = (p1.isDown && zones.right.rect.contains(p1.x, p1.y)) ||
                                  (p2.isDown && zones.right.rect.contains(p2.x, p2.y)) ||
                                  (pm.isDown && zones.right.rect.contains(pm.x, pm.y));
            zones.left.pressed =  (p1.isDown && zones.left.rect.contains(p1.x, p1.y)) ||
                                  (p2.isDown && zones.left.rect.contains(p2.x, p2.y)) ||
                                  (pm.isDown && zones.left.rect.contains(pm.x, pm.y));
        },

        create_editor_button: function() {
            var button = game.add.text(0, 0, 'Back to Editor');
            button.fixedToCamera = true;
            button.cameraOffset.setTo(128 + 196, 0);
            button.inputEnabled = true;
            button.priorityID = 1;
            button.input.useHandCursor = true;
            button.events.onInputDown.add(function() {
                game.state.start('Editor', true, false, niveau, niveauId);
            });
        },

        goLeft: function() {
            return cursors.left.isDown || zones.left.pressed;
        },

        goRight: function() {
            return cursors.right.isDown || zones.right.pressed;
        },
        
        goUp: function() {
            return cursors.up.isDown || jumpButton.isDown || zones.up.pressed;
        },

        update_touch_zones: function() {
            var width = game.scale.width;
            var height = layers.plateformes.height;

            zones.up.rect.setTo(0, 0, width, height / 2);
            zones.left.rect.setTo(0, height / 2, width / 3, height / 2);
            zones.right.rect.setTo(width * 2 / 3, height / 2, width / 3, height / 2);
        },

	update: function () {
		game.physics.arcade.collide(joueur, plateformes);			//collision avec le sol

		game.physics.arcade.overlap(joueur, layers.plateformes);		//collision avec "tous les blocs" : le noir en fait

	    var fps = game.time.fps;
	    this.updateFps(fps);

	    //Display the timer
	    minutes = Math.floor(counter/ 600) % 60;		//600 ds per s
	    seconds = Math.floor(counter / 10) % 60;		//10 ds per s
	    deciseconds = Math.floor(counter) % 10;
 
	    //If any of the digits becomes a single digit number, pad it with a zero
	    if (seconds < 10)
	        seconds = '0' + seconds;
	    if (minutes < 10)
	        minutes = '0' + minutes;
	   
	//    timer.setText('Temps: '+counter); 
	    timer.setText('Temps: '+minutes + ':'+ seconds + ':' + deciseconds);

            joueur.body.velocity.x = 0;

	    if (this.goRight()){
		//  Move to the right
		joueur.body.velocity.x = 250;
	    } else if (this.goLeft()) {
		joueur.body.velocity.x = -250;
	    }

	    //  Allow the joueur to jump if they are touching the ground.
	    if (this.goUp()){
	        //enemyFires();
	        if (joueur.body.touching.down || joueur.body.blocked.down) {
	            joueur.body.velocity.y = -380;
	            this.addScore(10);
	        };
	        if (double_saut == 0) {
	            joueur.body.velocity.y = -380;
	            this.addScore(10);
	            double_saut = 1;
	        };
	    };

	    if (!joueur.body.touching.down && !joueur.body.blocked.down && !this.goUp() && double_saut != 1){
	        double_saut = 0;
	    };

	    if (joueur.body.touching.down || joueur.body.blocked.down) {
	        double_saut = -1;
	    };
	},

	replay : function () {
	    this.updateScore(0);
    	    game.state.start('Game', true, false, niveauId, niveau, canGoBackToEditor);
	},

	getScore : function () {
	    return score;
	},

	updateScore : function (nouveau_score) {
	    score = nouveau_score;
	    scoreTexte.text = 'Score: ' + score;
	},

	addScore : function (plus) {
	    this.updateScore(this.getScore() + plus);
	},

	updateFps : function (fps) {
	    fpsTexte.text = 'FPS: ' + fps;
	},

	updateTimer : function () {
	    counter++;
	},

	slow : function () {
	//	if (joueur.body.touching.down || joueur.body.blocked.down) {
			joueur.body.velocity.x = 50;
		//	joueur.body.velocity.y = -380;
	//	}
	},

	finJeu : function (nom_sprite_resultat) {
	    joueur.kill();
	    menuWin = game.add.group();
	    var res = menuWin.create(0, 0, nom_sprite_resultat);
	    var reset = menuWin.create(0, 0, 'reset');
	    res.fixedToCamera = true;
	    res.cameraOffset.setTo(600, 25);
	    reset.fixedToCamera = true;
	    reset.cameraOffset.setTo(640, 25);
	    reset.inputEnabled = true;

	    reset.events.onInputUp.add(this.replay, this);

    	    timer.destroy();
	},

	gagnerJeu : function () {
            if (niveauId != null && !canGoBackToEditor) {
                request.post("/server/run", {niveau: niveauId, score: score});
            }
	    this.finJeu('win');
	},

	perdreJeu : function () {
	    this.finJeu('mort');
	}
};
};
