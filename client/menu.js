function Menu(game, common, request, auth) {
    var elements = [],
        cacheNiveauxPerso = null;

    return {

    preload : function() {
        game.load.spritesheet('menu1', 'assets/menu1.png', 266, 400, 2);
        game.load.spritesheet('menu2', 'assets/menu2.png', 266, 400, 2);
        game.load.spritesheet('menu3', 'assets/menu3.png', 266, 400, 2);
        game.load.spritesheet('withfriends','assets/withfriends.png',400,360,2);
        game.load.spritesheet('alone','assets/alone.png',400,360,2);
        game.load.image('fondChoixJoueurs','assets/fondchoixJoueurs.png');
        game.load.image('fondChoixCategorie','assets/fondchoixCategorie.png');
        game.load.image('credits','assets/credits.png');
        game.load.spritesheet('joueur','assets/joueur.png',800,40,2);
        game.load.spritesheet('categorie','assets/categorie.png',800,40,2);
        game.load.spritesheet('retour','assets/retour.png',800,40,2);
        game.load.spritesheet('nouveauNiveau','assets/nouveauNiveau.png',800,40,2);
        game.load.spritesheet('edit','assets/edit.png',80,40,2);
        game.load.spritesheet('submit','assets/submit.png',80,40,2);
    },

    resetElements: function() {
        elements.forEach(function(element) {
            element.destroy();
        });
        elements = [];
    },

    addE: function(e) {
        elements.push(e);
    },
        
    create: function () {
        this.resetElements();
        this.addE(game.add.button(0, 0, 'menu1', this.choixPlay, this, 1, 0));
        this.addE(game.add.button(266, 0, 'menu2', this.choixMake, this, 1, 0));
        this.addE(game.add.button(266*2, 0, 'menu3', this.choixCredits, this, 1, 0));
    },

    update : function() {
    },

    startGame : function(niveau) {
        game.state.start('Game', true, false, niveau.id, JSON.parse(niveau.data), false);
    },

    newLevel: function(niveau) {
        game.state.start('Editor', true, false);
    },

    editLevel: function(niveau) {
        game.state.start('Editor', true, false, JSON.parse(niveau.data), niveau.id);
    },
        
    choixPlay : function() {
        this.addE(game.add.button(0, 40, 'withfriends', this.choixPlayAmis, this, 1, 0));
        this.addE(game.add.button(400, 40, 'alone', this.choixPlaySeul, this, 1, 0));
        this.addE(game.add.button(0, 0, 'retour', this.create, this, 1, 0));
    },

    choixPlaySeul : function() {
        var menu = this;
        var getNiveaux = function() {
            request.get("/server/niveau/perso")
                .then(function(error, text, xhr) {
                    if (xhr.status == 200) {
                        var data = JSON.parse(text);
                        if (data.niveaux != undefined) {
                            cacheNiveauxPerso = data.niveaux;
                        }
                    }

                    if (cacheNiveauxPerso.length > 0) {
                        var i = 0;
                        cacheNiveauxPerso.forEach(function(niveau) {
                            this.addE(game.add.button(0, 40 + i * 40, 'joueur',function() {
                                this.startGame(niveau);
                            }, this, 1, 0));
                            this.addE(game.add.text(5, 43 + i * 40, niveau.name, { fontSize: '32px', fill: '#000' }));
                            i++;
                        }, menu);
                    } else {
                        this.addE(game.add.text(5, 43, "Hi " + auth.getName() + ", you did not create any level yet.", { fontSize: '25px', fill: '#000' }));
                        this.addE(game.add.text(5, 103, "Click here to make one.", { fontSize: '25px', fill: '#000' }));
                    }
                });
        };

        this.addE(game.add.sprite(0,0,'fondChoixJoueurs'));
        this.addE(game.add.button(0,0, 'retour', this.choixPlay, this, 1, 0));

        if (!auth.isAuthenticated()) {
            common.loginForm("Login to see you levels", auth,
                             function(userInfos) {
                                 getNiveaux();
                             },
                             this.choixPlay);
        } else {
            getNiveaux();
        }
    },

    choixPlayAmis : function() {
        var menu = this;
        request.get("/server/niveau/all")
            .then(function(error, text, xhr) {
                if (xhr.status == 200) {
                    var data = JSON.parse(text);
                    if (data.niveaux != undefined) {
                        cacheNiveauxPerso = data.niveaux;
                    }
                }

                if (cacheNiveauxPerso.length > 0) {
                    var i = 0;
                    cacheNiveauxPerso.forEach(function(niveau) {
                        this.addE(game.add.button(0,40 + 40 * i, 'joueur', function() {
                            this.startGame(niveau);
                        }, menu, 1, 0));
                        this.addE(game.add.text(5, 43 + 40 * i, niveau.creator_name, { fontSize: '32px', fill: '#000' }));
                        this.addE(game.add.text(250, 43 + 40 * i, niveau.name, { fontSize: '32px', fill: '#000' }));
                        this.addE(game.add.text(600, 43 + 40 * i, '', { fontSize: '32px', fill: '#000' }));
                        i++;
                    }, menu);
                } else {
                    this.addE(game.add.text(5, 43, "Hi " + auth.getName() + ", you did not create any level yet.", { fontSize: '25px', fill: '#000' }));
                    this.addE(game.add.text(5, 103, "Click here to make one.", { fontSize: '25px', fill: '#000' }));
                }
            });

        this.addE(game.add.sprite(0,0,'fondChoixJoueurs'));
        this.addE(game.add.button(0,0, 'retour', this.choixPlay, this, 1, 0));
    },

    choixMake : function() {
        if (auth.isAuthenticated()) {
            var menu = this;

            request.get("/server/niveau/perso")
                .then(function(error, text, xhr) {
                    if (xhr.status == 200) {
                        var data = JSON.parse(text);
                        if (data.niveaux != undefined) {
                            cacheNiveauxPerso = data.niveaux;
                        }
                    }

                    if (cacheNiveauxPerso.length > 0) {
                        var i = 0;
                        cacheNiveauxPerso.forEach(function(niveau) {
                            this.addE(game.add.sprite(0,40 + 40 * i, 'categorie'));
                            this.addE(game.add.text(5, 43 + 40 * i, niveau.name, { fontSize: '32px', fill: '#000' }));
            
                            this.addE(game.add.button(800-80,40 + 40 * i,'edit', function() {
                                this.editLevel(niveau);
                            }, this, 1, 0));
                            // this.addE(game.add.button(800-160,40 + 40 * i,'submit', this.startGame, this, 1, 0));
                            i++;
                        }, menu);
                    } else {
                        this.addE(game.add.text(5, 43, "Hi " + auth.getName() + ", you did not create any level yet.", { fontSize: '25px', fill: '#000' }));
                        this.addE(game.add.text(5, 103, "Click here to make one.", { fontSize: '25px', fill: '#000' }));
                    }
                });
            
            this.addE(game.add.sprite(0,0,'fondChoixCategorie'));
            this.addE(game.add.button(0,0, 'retour', this.create, this, 1, 0));

            this.addE(game.add.button(0,360,'nouveauNiveau', this.newLevel, 1, 0));
        } else {
            this.newLevel();
        }
    },

    choixCredits : function() {
    	this.addE(game.add.sprite(0,0,'credits'));
    	this.addE(game.add.button(0,0, 'retour', this.create, this, 1, 0));
    }

};
};
