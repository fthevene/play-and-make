var common = {
    BLOCKS_X: 80,
    BLOCKS_Y: 12,
    VISIBLE_X: 50,
    BLOCK_SIZE: 32,

    EFFECTS: {
        win: "win",
        lose: "lose",
    },

    BACKGROUND: '#c3c3c3',
};

common.BLOCKS = {
    1: {name: "Fin gagnante", tilename: "but", effect: common.EFFECTS.win},
    2: {name: "Mortel", tilename: "mechant", effect: common.EFFECTS.lose},
    0: {name: "Bloc Noir", tilename: "bloc", effect: null},
    3: {name: "Bloc Blanc", tilename: "bloc_blanc", effect: null},
    4: {name: "Dirt", tilename: "dirt", effect: null},
};

common.preload_blocks = function(game) {
    game.load.image('bloc', 'assets/bloc.png');
    game.load.image('bloc_blanc', 'assets/bloc2.png');
    game.load.image('but', 'assets/but.png');
    game.load.image('mechant', 'assets/mechant.png');
    game.load.image('dirt', 'assets/dirt.png');
    game.load.image('sol', 'assets/sol.png');
    game.load.image('balleSimple', 'assets/balleSimple.png');
};

common.create_blocks = function(map) {
    var bid;
    var dblocks = Object.keys(this.BLOCKS);
    for (var bi = 0; bi < dblocks.length; bi++) {
        bid = dblocks[bi];
        map.addTilesetImage(this.BLOCKS[bid].tilename, this.BLOCKS[bid].tilename, this.BLOCK_SIZE, this.BLOCK_SIZE, 0, 0, bid);
    }
};

common.create_sol = function(plateformes) {
    var sol = plateformes.create(0, (common.BLOCKS_Y - 1) * common.BLOCK_SIZE, 'sol');
    sol.body.immovable = true;
    plateformes.create(1300, (common.BLOCKS_Y - 1) * common.BLOCK_SIZE, 'sol').body.immovable = true;
};

common.create_niveau = function(map, layer, niveau) {
    var tile, block;
    for(var ti = 0; ti < niveau.length; ti++) {
	tile = niveau[ti];
	block = common.BLOCKS[tile[2]];

	if (block == undefined) {
	    continue;
        }

	map.putTile(tile[2], tile[0], tile[1], layer);
    }
};

common.loginForm = function(message, auth, succesCallback, cancelCallback) {
    // display form
    var form = document.getElementById("loginRegisterForm");
    form.className = "";

    // reset error
    var errorDisplay = document.getElementById("loginRegisterError");
    errorDisplay.innerHTML = "";
    
    // change buttons action
    document.getElementById("userRegisterButton").onclick = function() {
        var name = document.getElementById("userRegisterName");
        var email = document.getElementById("userRegisterEmail");
        auth.register(name.value, email.value, function(error, data) {
            if (!error) {
                succesCallback(data);
                form.className = "hidden";
            } else if (error == 400) {
                if (data.empty == "email") {
                    window.setTimeout(function() { email.focus(); }, 0);
                    errorDisplay.innerHTML = "Please input your email.";
                } else if (data.empty == "name") {
                    window.setTimeout(function() { name.focus(); }, 0);
                    errorDisplay.innerHTML = "Please input your nickname.";
                }
            } else if (error == 409) {
                errorDisplay.innerHTML = "This nickname or email is already used, could you choose another one ?";
            }
        });
    };

    document.getElementById("cancelRegisterButton").onclick = function() {
        form.className = "hidden";
        if (cancelCallback != null) {
            cancelCallback();
        }
    };
};
