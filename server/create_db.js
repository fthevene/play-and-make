module.exports = function (db) {
    db.serialize(() => {
        db.run(`CREATE TABLE IF NOT EXISTS player (
                   id INTEGER PRIMARY KEY,
                   name TEXT NOT NULL UNIQUE,
                   email TEXT NOT NULL UNIQUE,
                   password TEXT NOT NULL,
                   registered_at TEXT NOT NULL,
                   verified_at TEXT
               )`); // pk naturelle = name

        db.run(`CREATE TABLE IF NOT EXISTS niveau (
                   id INTEGER PRIMARY KEY,
                   creator INTEGER REFERENCES player(id),
                   name TEXT NOT NULL,
                   category TEXT NOT NULL,
                   data TEXT NOT NULL,
                   replaced_by INTEGER
               )`); // pk naturelle = (creator, name, category)
        // replaced_by null quand le niveau n'a pas été remplacé

        db.run(`CREATE TABLE IF NOT EXISTS run (
                   id INTEGER PRIMARY KEY,
                   player INTEGER REFERENCES player(id),
                   niveau INTEGER REFERENCES niveau(id),
                   score INTEGER NOT NULL,
                   date TEXT NOT NULL
               )`); // pk naturelle = (player, date)
    });
};
