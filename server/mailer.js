const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');

var transporter = nodemailer.createTransport(smtpTransport({
    host: 'localhost',
    port: 25,
    auth: {
        user: 'username',
        pass: 'password'
    } 
}, {
    // default values for transporter.sendMail
    from: 'Play and Make <noreply@play-and-make.xn--thvenet.fr>'
}));

function formatFriendInvite(friendEmail, levelId, playerEmail, playerName) {
    return playerName + " t'invite à jouer sur son niveau http://play-and-make.xn--thvenet.fr#play=" + levelId + "/from=" + playerEmail + "/to=" + friendEmail;
}

function inviteFriend(transporter, friendEmail,  playerEmail, message, subject) {
    transporter.sendMail({
        to: friendEmail,
        replyTo: playerEmail,
        subject: subject,
        text: message
    });
};

module.exports = {inviteFriend, formatFriendInvite};
