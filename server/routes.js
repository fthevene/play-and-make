const login = require("./login");
const runs = require("./runs");
const niveaux = require("./niveaux");

const debug = require("debug")("routing");

module.exports = function(app, db) {
    app.get('/', function (req, res) {
        res.json({home: true});
    });

    // TODO: handle cache duration everywhere

    // gestion des utilisateurs
    function now() {
        return new Date().toISOString();
    }

    app.post('/user/register', function(req, res) {
        if (req.body.name == undefined || req.body.name == "") {
            res.status(400).json({empty: "name"});
        } else if (req.body.email == undefined || req.body.email == "") {
            res.status(400).json({empty: "email"});
        } else {
            login.createUser(db, req.body.name, req.body.email, now(), (id, error) => {
                if (error == null) {
                    // TODO: send email
                    login.setAuthCookie(res, id);
                    res.status(201).end();
                } else {
                    debug(error);
                    res.set({'Content-type': 'text/plain'}).status(409).end(); // duplicate
                }
            });
        }
    });

    app.get('/user/verif/:code', function(req, res) {
        // TODO, check if validation code in table
    });

    app.get('/user/logout', function(req, res) {
        login.removeAuthCookie(res);
        res.end();
    });

    app.post('/user/login', function(req, res) {
        login.checkUser(db, req.body.login, req.body.password, (id) => {
            if (id) {
                login.setAuthCookie(res, id);
                res.end();
            } else {
                res.status(401).end();
            }
        });
    });
    
    app.get('/user/info', login.requireLogin, function(req, res) {
        login.getInfos(db, req.user, function(name, email) {
            res.json({name: name, email: email});
        });
    });

    // gestion des niveaux

    app.get('/niveau/all', function(req, res) {
        niveaux.getActives(db, (niveaux, error) => {
            if (error) {
                debug(error);
                res.status(400).send();
            } else {
                res.json({niveaux});
            }
        });
    });

    app.get('/niveau/perso', login.requireLogin, function(req, res) {
        niveaux.getActivesWithCreator(db, req.user, (niveaux, error) => {
            if (error) {
                debug(error);
                res.status(400).send();
            } else {
                res.json({niveaux});
            }
        });
    });

    app.post('/niveau', login.requireLogin, function(req, res, data) {
        niveaux.addNiveau(db, req.user, req.body.name, req.body.categorie, req.body.data,
                          (id,  error) => {
                              if (error == null) {
                                  res.status(201).json({id});
                              } else {
                                  debug(error);
                                  res.status(400).send(error);
                              }
                          });
    });

    app.post('/niveau/:id', login.requireLogin, function(req, res) {
        niveaux.updateNiveau(db, req.user, req.params.id, req.body.data, (error) => {
            if (error == null) {
                res.status(200).json({});
            } else if (error == "bad user") {
                res.status(403).json({});
            } else {
                res.status(500).json({});
            }
        });
    });
    
    // gestion des runs
    app.post('/run', login.requireLogin, function(req, res) {
        runs.addRun(db, req.user, req.body.niveau, req.body.score, now(), (id, error) => {
            if (error == null) {
                res.status(201).json({id});
            } else {
                debug(error);
                res.status(400).json({}); // TODO: retourner la raison de l'erreur
            }
        });
    });

    app.get('/run/best/:niveau', function(req, res) {
        runs.bestRunLevel(db, req.params.niveau, (found, score, name, date) => {
            if (found) {
                res.json({niveau: req.params.niveau, score: score, playerName: name, date: date});
            } else {
                res.status(204).end();
            }
        });
    });

    app.get('/run/perso/:niveau', login.requireLogin, function(req, res) {

    });
};
