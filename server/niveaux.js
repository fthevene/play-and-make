function addNiveau(db, creator, name, categorie, data, callback) {
    db.run("INSERT INTO niveau (creator, name, category, data, replaced_by) VALUES ($creator, $name, $category, $data, NULL)",
           {$creator: creator, $name: name, $category: categorie, $data: JSON.stringify(data)},
           function(error) {
               callback(this.lastID, error);
           });
};

function updateNiveau(db, user, niveauId, data, callback) {
    db.get("SELECT creator FROM niveau WHERE id = $id",
           {$id: niveauId},
           function(error, row) {
               if (!error && row.creator == user) {
                   db.run("UPDATE niveau SET data = $data WHERE id = $id",
                          {$data: JSON.stringify(data), $id: niveauId},
                          function(error) {
                              callback(error);
                          });
               } else {
                   callback("bad user");
               }
           });
}

function getActives(db, callback) {
    db.all("SELECT n.id, n.name, n.category, n.data, p.name AS creator_name FROM niveau n JOIN player p ON p.id = n.creator WHERE n.replaced_by IS NULL", {},
           (err, rows) => callback(rows, err));
}

function getActivesWithCreator(db, creator, callback) {
    db.all("SELECT n.id, n.name, n.category, n.data, p.name AS creator_name FROM niveau n JOIN player p ON p.id = n.creator WHERE n.replaced_by IS NULL AND n.creator = $creator",
           {$creator: creator},
           (err, rows) => callback(rows, err));
}

module.exports = {addNiveau, getActives, getActivesWithCreator, updateNiveau};
