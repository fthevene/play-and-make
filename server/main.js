const sqlite = require('sqlite3');
const express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

const create_db = require('./create_db');

if (!process.env.COOKIE_SECRET) {
    console.log("Please set the COOKIE_SECRET environment variable");
    process.exit(-1);
}

// Setup database
const db = new sqlite.Database("play_and_make.db", null, (error) => {
    if (error === null) {
        db.run("PRAGMA foreign_keys = true"); // actually enforce foreign key constraints
    } else {
        console.log("Unable to open db : " + error);
        process.exit(-1);
    }
});
create_db(db);

// Setup http server
const app = express();
app.use(cookieParser(process.env.COOKIE_SECRET));
app.use(bodyParser.json());

require('./express_config')(app);
require('./routes')(app, db);

const server = app.listen(3028, function () {
    const host = server.address().address;
    const port = server.address().port;

    console.log('* Play&Make server listening at http://%s:%s', host, port);
});

// catch SIGTERM and shutdown gracefully
require('./win-sigint')();
function shutdown() {
    console.log('* Closing http server');
    server.close(() => {
        console.log('* Closing database connection');
        db.close(() => process.exit(0));
    });
}
process.on('SIGTERM', shutdown);
process.on('SIGINT', shutdown);
