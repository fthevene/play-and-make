function addRun(db, player, level, score, date, callback) {
    db.run("INSERT INTO run (player, niveau, score, date) VALUES ($player, $niveau, $score, $date)",
           {$player: player, $niveau: level, $score: score, $date: date},
           function(error) {
               callback(this.lastId, error);
           });
}

function bestRunLevel(db, level, callback) {
    db.get("SELECT r.score, r.date, p.name FROM run r JOIN player p ON p.player = WHERE r.level = $level ORDER BY r.score DESC LIMIT 1",
           {$level: level},
           function(error, row) {
               if (row) {
                   callback(true, row.score, row.name, row.date);
               } else {
                   callback(false);
               }
           });
};

module.exports = {addRun, bestRunLevel};
