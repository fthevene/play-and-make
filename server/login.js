const crypto = require('crypto');
const debug = require('debug')('login');

var AUTH_COOKIE_NAME = "auth";

function createUser(db, name, email, date, callback) {
    // pas de mot passe par défaut
    db.run("INSERT INTO player (name, email, password, registered_at) VALUES ($name, $email, '', $date)",
           {$name: name, $email: email, $date: date},
           function(error) {
               callback(this.lastID, error);
           });
}

function hashPassword(pass) {
    // TODO: 
}

function checkPassword(hash, pass) {
    // TODO: 
}

function checkUser(db, login, password, callback) {
    db.get("SELECT id, email, password FROM player WHERE name = $login",
           {$login: login},
           function(err, row) {
               if (row != undefined) {
                   callback(row.id);
               } else {
                   callback();
               }
           });
}

function getAuthCookie(req) {
    return req.signedCookies[AUTH_COOKIE_NAME];
}

function setAuthCookie(res, auth) {
    res.cookie(AUTH_COOKIE_NAME, auth, { httpOnly: true,
                                         maxAge: 1000 * 3600 * 24 * 31 * 2,
                                         signed: true
                                       });
}

function removeAuthCookie(res) {
    res.clearCookie("auth");
}

function getInfos(db, userId, callback) {
    db.get("SELECT name, email FROM player WHERE id = $id",
           {$id: userId},
           function(error, row) {
               callback(row.name, row.email);
           });
}

function requireLogin(req, res, next) {
    // a user id should be in auth cookie
    const cookie = getAuthCookie(req);
    if (cookie === undefined) {
        res.set({'Content-type': 'text/plain'}).status(401).end();
    } else {
        req.user = cookie;
        next();
    }
};

module.exports = {requireLogin, setAuthCookie, removeAuthCookie, createUser, getInfos};
